<?php

class convertXMLtoArray
{
	private $getArray;

	public function __construct($xml_response)
	{
		$xml            = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xml_response);
		$xml            = simplexml_load_string($xml);
		$json           = json_encode($xml);
		$this->getArray = json_decode($json);
	}

	public function getDate()
	{
		$array  = $this->getArray;
		$valor  = $array->sBody->GetCalendarDatesResponse->GetCalendarDatesResult->adateTime;
		$result = [];
		foreach ($valor as $datetime) {
			$result[] = str_replace('T00:00:00', '', $datetime);
		}
		return $result;
	}
}