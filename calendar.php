<?php

include 'xmlToArray.php';

/**
 * Arquivo de configuração para a chamada das datas
 * @package Soap Calendar Config
 * @author  web.andrade@hotmail.com | Rafael Andrade
 * 
 * $wsdl
 * Endereço padrão onde se encontra o serviço de retorno para as datas
 * @var string
 */
$wsdl = 'http://cloud.multiclubes.com.br/rqr/Services/Cloud/Extensions/RQR/Calendar.svc?wsdl';

/**
 * $saction
 * Local onde se encontra a ação "GetCalendarDates", com este endereço
 * garantimos que a classe soapClient possa definir onde será disparado o método
 * @var string
 */
$saction = 'http://loja.multiclubes.com.br/extensions/rqr/calendar/IService/GetCalendarDates';

/**
 * Começa aqui as opções de envio para a classe soapClient php
 * @var array
 */
$soap_options                       = [];
$soap_options["connection_timeout"] = 25;
$soap_options["location"]           = $wsdl;
$soap_options["uri"]                = "http://tempuri.org/";
$soap_options['trace']              = TRUE;
$soap_options['cache_wsdl']         = WSDL_CACHE_NONE;
$soap_options['soap_version']       = SOAP_1_2;
$soap_options["exceptions"]         = true;
$xml_request                        = file_get_contents('Calendar.xml');

try
{
    $client = new SoapClient($wsdl, $soap_options);
    $result = $client->__doRequest($xml_request, $wsdl, $saction, SOAP_1_1);

    $rep1   = str_replace('--uuid:1d1ec4ff-0b37-4980-9689-4b5017e43ecc+', '', $result);
    $rep2   = str_replace('Content-ID: <http://tempuri.org/0>', '', $rep1);
    $rep3   = str_replace('Content-Transfer-Encoding: 8bit', '', $rep2);
    $rep4   = str_replace('Content-Type: application/xop+xml;charset=utf-8;type="text/xml"', '', $rep3);
    $rep5   = str_replace('id=', '', $rep4);
    $rep6   = str_replace('--', '', $rep5);
    $rep7   = str_replace("\n", '', $rep6);
    $rep8   = preg_replace('/^.*?<s/', '<s', $rep7);
    $rep9   = preg_replace('/Envelope>.*?$/', 'Envelope>', $rep8);
    $object = new convertXMLtoArray($rep9);

    print_r($result);
    // print_r($rep9);
    // print_r($object);
    // print_r($object->getDate());
} catch(SoapFault $e) {
	echo $e;
}