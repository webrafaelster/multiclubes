# Integração Multi Clubes

Código simples para referência utilizando a classe SoapClient do php a fim de obter uma lista de datas do sistema de vendas mediante webservice.

Saída completa:
````php
    --uuid:68d9a56d-758f-4ac7-8dca-7c1dd1882dac+id=8026
    Content-ID: <http://tempuri.org/0>
    Content-Transfer-Encoding: 8bit
    Content-Type: application/xop+xml;charset=utf-8;type="text/xml"

    <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetCalendarDatesResponse xmlns="http://loja.multiclubes.com.br/extensions/rqr/calendar"><GetCalendarDatesResult xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:dateTime>2018-07-26T00:00:00</a:dateTime><a:dateTime>2018-07-27T00:00:00</a:dateTime><a:dateTime>2018-07-28T00:00:00</a:dateTime><a:dateTime>2018-07-29T00:00:00</a:dateTime><a:dateTime>2018-07-30T00:00:00</a:dateTime><a:dateTime>2018-07-31T00:00:00</a:dateTime><a:dateTime>2018-08-01T00:00:00</a:dateTime><a:dateTime>2018-08-03T00:00:00</a:dateTime><a:dateTime>2018-08-04T00:00:00</a:dateTime><a:dateTime>2018-08-05T00:00:00</a:dateTime><a:dateTime>2018-08-06T00:00:00</a:dateTime><a:dateTime>2018-08-07T00:00:00</a:dateTime><a:dateTime>2018-08-08T00:00:00</a:dateTime></GetCalendarDatesResult></GetCalendarDatesResponse></s:Body></s:Envelope>
    --uuid:68d9a56d-758f-4ac7-8dca-7c1dd1882dac+id=8026--
````